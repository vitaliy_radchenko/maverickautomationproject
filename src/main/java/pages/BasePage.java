package pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
    WebDriver driver;
    WebDriverWait waitFor;
    Logger logger;

    public BasePage(WebDriver driver){
        this.driver = driver;
        waitFor = new WebDriverWait(driver, 10);
    }

}
