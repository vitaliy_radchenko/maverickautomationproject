package pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class UserLoggedInPage extends BasePage {
    public UserLoggedInPage(WebDriver driver) {
        super(driver);
        logger = Logger.getLogger(UserLoggedInPage.class);
    }

    public boolean isUserLoggedIn(){
        passGmailGreetings();

        try{
            driver.findElement(By.xpath("//div[@id=':j']/span[contains(text(), 'Gmail')]"));
            return true;
        } catch (NoSuchElementException exception){
            return false;
        }
    }

    private void passGmailGreetings(){
       if (driver.findElements(By.xpath("//div[@class='Nb-Pb']")).size() > 0){
            driver.findElement(By.xpath("//a[@data-ved='0CAIQvSc']")).click();
           waitFor.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//ul[@class='gb_P gb_H']")));
           driver.findElement(By.xpath("//a[@href='https://mail.google.com/mail/']")).click();
           logger.info("Passed Gmail Instruction");
        }
    }

}
