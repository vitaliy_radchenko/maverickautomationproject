package pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GmailHomePage extends BasePage{
    public GmailHomePage(WebDriver driver) {
        super(driver);
        logger = Logger.getLogger(GmailHomePage.class);
    }

    private static By emailInput = By.id("Email");
    private static By passwordInput = By.id("Passwd");
    private static By singInButton = By.id("signIn");
    private static By signedInCheckBox = By.id("PersistentCookie");

    public void open(){
        driver.get("https://accounts.google.com");
        logger.info("Home Page was opened");
    }

    public UserLoggedInPage logInToTheAccount(String email, String password, boolean staySignedIn){
        driver.findElement(emailInput).clear();
        driver.findElement(emailInput).sendKeys(email);
        logger.info("Entered " + email + " in " + emailInput);

        driver.findElement(passwordInput).clear();
        driver.findElement(passwordInput).sendKeys(password);
        logger.info("Entered " + password + " in " + passwordInput);

        if (staySignedIn)
            checkSignedInCheckBox();
        else
            unCheckSignedInCheckBox();

        driver.findElement(singInButton).click();
        logger.info("Clicked singInButton");

        return new UserLoggedInPage(driver);
    }

    private void checkSignedInCheckBox(){
        if (!driver.findElement(signedInCheckBox).isSelected()){
            driver.findElement(signedInCheckBox).click();
            logger.info("SignedInCheckBox was checked.");
        } else
            logger.info("SignedInCheckBox is already checked");
    }

    private void unCheckSignedInCheckBox(){
        if (driver.findElement(signedInCheckBox).isSelected()){
            driver.findElement(signedInCheckBox).click();
            logger.info("SignedInCheckBox was unchecked");
        } else
            logger.info("SignedInCheckBox is already unchecked");
    }

}
