package logInTests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.GmailHomePage;
import pages.UserLoggedInPage;

import java.util.concurrent.TimeUnit;

public class LogInTest {
    WebDriver driver;

    @BeforeMethod
    public void setUp(){
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Test
    public void positiveLogInTest(){
        GmailHomePage gmailHomePage = new GmailHomePage(driver);
        gmailHomePage.open();
        UserLoggedInPage userLoggedInPage = gmailHomePage.logInToTheAccount("testmaverickautomation", "testMaverickAutomation1", false);
        Assert.assertTrue(userLoggedInPage.isUserLoggedIn());
    }

    @AfterMethod
    public void tearDown(){
        driver.quit();
    }

}
